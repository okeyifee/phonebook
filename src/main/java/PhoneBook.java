import java.util.*;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.stream.Collectors;


/**
 * A Basic PhoneBook
 *
 * Assumptions Made:
 * i)  phone Numbers are unique
 * ii) Just like our typical phoneBook, the combination of firstName and LastName are unique
 *      i.e two user can have same firstName but different lastName or vice versa.
 */
public class PhoneBook {

    public Map<String, Person> phoneBook = new HashMap<>();

    /**
     * Store phoneDetails
     *
     * @param person the details to be saved
     * @return the phone number if successful or
     * null (if phoneNumber already exists)
     */
    public void addToPhoneBook(Person person) {
        if ( person != null && null != person.getPhoneNumber() && null != person.getFirstName() && null != person.getLastName()  &&
                !phoneBook.containsKey(person.getPhoneNumber()) && !checkIfPhoneBookHasPerson(person)) {
            phoneBook.put(person.getPhoneNumber(), person);
        }
    }

    /**
     * get phoneDetails
     *
     * @param phoneNumber the phoneNumber of the phoneDetails to be retreived
     * @return the phone Details if successful or
     * null (if phoneNumber Does not exists)
     */
    public Person getPhoneDetails(String phoneNumber) {
        return phoneBook.get(phoneNumber);
    }

    /**
     * getAllPhoneNumbers saved
     *
     * @return list of phone Numbers if exist or
     * an empty list (if phoneNumber Does not exists)
     */
    public List<String> getAllPhoneNumbers() {
        return phoneBook.keySet().stream().collect(Collectors.toList());
    }

    public boolean checkIfPhoneBookHasPerson(Person person){
        AtomicBoolean hasName = new AtomicBoolean(false);
        phoneBook.forEach((phone, person1) -> {
            if(person1.equals(person))  hasName.set(true);
        });
        return hasName.get();
    }
}
