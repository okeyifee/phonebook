import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

import java.util.List;

import static org.junit.jupiter.api.Assertions.assertAll;
import static org.junit.jupiter.api.Assertions.assertEquals;

class PhoneBookTest {

    private Person person;
    private PhoneBook phoneBook;

    @BeforeEach
    public void setUp() {
        person = new Person();
        phoneBook = new PhoneBook();
    }

    @Test
    void addToPhoneBookShould_notSavePhoneDetails_whenPhoneNumberOrFirstNameOrLastNameIsNull() {
        phoneBook.addToPhoneBook(new Person(null, "okezie", "okechukwu"));
        List<String> phoneNumbers = phoneBook.getAllPhoneNumbers();
        assertEquals(0, phoneNumbers.size());
    }

    @Test
    void addToPhoneBookShould_notSavePhoneDetails_whenPhoneNumberAlreadyExist_andFirstNameAndLastNameDoesNotExist() {
        phoneBook.addToPhoneBook(new Person("07060816203", "okezie", "okechukwu"));
        // retrieve list and verify size
        List<String> before = phoneBook.getAllPhoneNumbers();
        assertEquals(1, before.size());
        // save the phoneDetails and verify it's not added
        phoneBook.addToPhoneBook(new Person("07060816203", "okeziep", "okechukwup"));
        List<String> after = phoneBook.getAllPhoneNumbers();
        assertEquals(1, after.size());
    }

    @Test
    void addToPhoneBookShould_notSavePhoneDetails_whenPhoneNumberAlreadyExist_andBothFirstNameAndLastNameAlreadyExist() {
        phoneBook.addToPhoneBook(new Person("07060816203", "okezie", "okechukwu"));
        // retrieve list and verify size
        List<String> before = phoneBook.getAllPhoneNumbers();
        assertEquals(1, before.size());
        // save the phoneDetails and verify it's not added
        phoneBook.addToPhoneBook(new Person("07060816203", "okezie", "okechukwu"));
        List<String> after = phoneBook.getAllPhoneNumbers();
        assertEquals(1, after.size());
    }

    @Test
    void addToPhoneBookShould_notSavePhoneDetails_whenPhoneNumberAlreadyExist_andFirstNameExistAndLastNameDoesNotExist() {
        phoneBook.addToPhoneBook(new Person("07060816203", "okezie", "okechukwu"));
        // retrieve list and verify size
        List<String> before = phoneBook.getAllPhoneNumbers();
        assertEquals(1, before.size());
        // save the phoneDetails and verify it's not added
        phoneBook.addToPhoneBook(new Person("07060816203", "okezie", "okechukwup"));
        List<String> after = phoneBook.getAllPhoneNumbers();
        assertEquals(1, after.size());
    }

    @Test
    void addToPhoneBookShould_notSavePhoneDetails_whenPhoneNumberAlreadyExist_andFirstNameDoesNotExistAndLastNameExist() {
        phoneBook.addToPhoneBook(new Person("07060816203", "okezie", "okechukwu"));
        // retrieve list and verify size
        List<String> before = phoneBook.getAllPhoneNumbers();
        assertEquals(1, before.size());
        // save the phoneDetails and verify it's not added
        phoneBook.addToPhoneBook(new Person("07060816203", "okeziep", "okechukwu"));
        List<String> after = phoneBook.getAllPhoneNumbers();
        assertEquals(1, after.size());
    }

    @Test
    void addToPhoneBookShould_savePhoneDetails_whenPhoneNumberDoesNotExist_andFirstNameDoesNotExistAndLastNameExist() {
        phoneBook.addToPhoneBook(new Person("07060816203", "okezie", "okechukwu"));
        // retrieve list and verify size
        List<String> before = phoneBook.getAllPhoneNumbers();
        assertEquals(1, before.size());
        // save the phoneDetails and verify it's not added
        phoneBook.addToPhoneBook(new Person("07060816206", "okeziep", "okechukwu"));
        List<String> after = phoneBook.getAllPhoneNumbers();
        assertEquals(2, after.size());
    }

    @Test
    void addToPhoneBookShould_savePhoneDetails_whenPhoneNumberDoesNotExist_andFirstNameExistAndLastNameDoesNotExist() {
        phoneBook.addToPhoneBook(new Person("07060816203", "okezie", "okechukwu"));
        // retrieve list and verify size
        List<String> before = phoneBook.getAllPhoneNumbers();
        assertEquals(1, before.size());
        // save the phoneDetails and verify it's not added
        phoneBook.addToPhoneBook(new Person("07060816209", "okezie", "okechukwup"));
        List<String> after = phoneBook.getAllPhoneNumbers();
        assertEquals(2, after.size());
    }

    @Test
    void addToPhoneBookShould_savePhoneDetails_whenPhoneNumberDoesNotExist_andFirstNameDoesNotExistAndLastNameDoesNotExist() {
        phoneBook.addToPhoneBook(new Person("07060816203", "okezie", "okechukwu"));
        // retrieve list and verify size
        List<String> before = phoneBook.getAllPhoneNumbers();
        assertEquals(1, before.size());
        // save the phoneDetails and verify it's not added
        phoneBook.addToPhoneBook(new Person("07060816208", "okeziep", "okechukwup"));
        List<String> after = phoneBook.getAllPhoneNumbers();
        assertEquals(2, after.size());
    }

    @Test
    void getPhoneDetails_shouldReturn_null_when_phoneNumberIsNull() {
        Person phoneDetails = phoneBook.getPhoneDetails(null);
        assertEquals(null, phoneDetails);
    }

    @Test
    void getPhoneDetails_shouldReturn_null_when_phoneNumberIsNotSaved() {
        Person phoneDetails = phoneBook.getPhoneDetails("07060816203");
        assertEquals(null, phoneDetails);
    }

    @Test
    void getPhoneDetails_shouldReturn_phoneDetails_when_phoneNumberExists() {
        person.setPhoneNumber("07060816203");
        person.setFirstName("samuel");
        person.setLastName("okechukwu");
        phoneBook.addToPhoneBook(person);

        Person phoneDetails = phoneBook.getPhoneDetails("07060816203");
        assertAll("Should return the phone Details",
                () -> assertEquals("07060816203", phoneDetails.getPhoneNumber()),
                () -> assertEquals("samuel", phoneDetails.getFirstName()),
                () -> assertEquals("okechukwu", phoneDetails.getLastName())
        );
    }

    @Test
    @DisplayName("Returns a list of all saved phoneNumbers")
    void getAllPhoneNumbers_shouldReturn_ListOfPhoneNumbers() {
        phoneBook.addToPhoneBook(new Person("07060816203", "okezie", "okechukwu"));
        phoneBook.addToPhoneBook(new Person("07060816204", "samuel", "patrick"));

        List<String> phoneNumbers = phoneBook.getAllPhoneNumbers();
        assertAll("Should return list phone Numbers",
                () -> assertEquals(2, phoneNumbers.size()),
                () -> assertEquals(true, phoneNumbers.contains("07060816203"))
        );
    }

    @Test
    @DisplayName("Returns an empty list")
    void getAllPhoneNumbers_shouldReturn_EmptyList() {
        List<String> phoneNumbers = phoneBook.getAllPhoneNumbers();
        assertEquals(0, phoneNumbers.size());
    }
}